readonly LOCAL_BIN="${HOME}/.local/bin"

if [ ! -e "${LOCAL_BIN}" ]; then
    mkdir -p "${LOCAL_BIN}"
fi

if [ -z ${path[(r)${LOCAL_BIN}]:+x} ]; then
    export PATH=${PATH}:${LOCAL_BIN}
fi

readonly PROJECT="${0:A:h}"

(
    cd "${PROJECT}"

    if ! pipenv --venv >&/dev/null; then
        pipenv install
    fi
)

readonly COMMAND="${LOCAL_BIN}/kaggle"

if [ ! -e "${COMMAND}" ]; then
    ln -s "${PROJECT}/bin/runner" "${COMMAND}"
fi
