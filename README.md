Kaggle Cli Pipenv Wrapper
=========================

This is a simple demonstration of using a pipenv and a script to run a command within an environment.

Antigen
-------

You can install this with [antigen](https://github.com/zsh-users/antigen):

```bash
antigen bundle https://gitlab.com/matthewfranglen/kagglecli-pipenv-wrapper.git
```

This makes the `kaggle` command available.

Synopsis
--------

This assumes that `~/.local/bin` is in your `$PATH` _after_ the pipenv entries.

```bash
➜ git clone git@gitlab.com:matthewfranglen/kagglecli-pipenv-wrapper.git
➜ cd kagglecli-pipenv-wrapper
➜ pipenv install
➜ ln -s "${PWD}/bin/runner" ~/.local/bin/kaggle
➜ kaggle
usage: kaggle [options] <command> <subcommand> [<subcommand> ...] [parameters]
To see help text, you can run:

  kaggle help
  kaggle <command> help
  kaggle <command> <subcommand> help
kaggle: error: the following arguments are required: command
```
